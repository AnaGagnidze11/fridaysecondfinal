package com.example.fridaytwo

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_second.*

class SecondActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_second)
        init()
    }


    private fun init(){
        val email = intent.extras?.getString("email", "")
        val firstName = intent.extras?.getString("firstName", "")
        val lastName = intent.extras?.getString("lastName", "")
        val age = intent.extras?.getString("age", "")

        emailTxtV.text = email
        firstNameTxtV.text = firstName
        lastNameTxtV.text = lastName
        ageTxtV.text = age

    }
}