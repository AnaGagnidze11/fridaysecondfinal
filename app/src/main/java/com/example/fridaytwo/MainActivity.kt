package com.example.fridaytwo

import android.content.Intent
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {


    //  MutableMap is created to save Users, where keys are their emails and values are User class objects
    private val userList: MutableMap<String, User> = mutableMapOf()

    //  These two variables are used to cound removed and active users
    private var activeUserNum = 0
    private var removedUserNum = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()
    }


    private fun init() {
        addButton.setOnClickListener {
            addUser()
        }

        removeButton.setOnClickListener {
            removeUser()
        }

        updateButton.setOnClickListener {
            updateUser()
        }

        showInfoButton.setOnClickListener {
            showInfo()
        }

    }

    private fun addUser() {

        val email = emailEdT.text.toString()
        val firstName = firstNameEdT.text.toString()
        val lastName = lastNameEdT.text.toString()
        val age = ageEdT.text.toString()

//      This function checks if user inputs are empty or not, then validates the number and lastly,
//      checks if there already is User in a Map, and then adds the user if not, doesn't add
//      if yes.

        if (email.isNotEmpty() && firstName.isNotEmpty() && lastName.isNotEmpty() && age.isNotEmpty()) {
            if (android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
                val user = User(email, firstName, lastName, age.toInt())
                if (email in userList.keys) {
                    Toast.makeText(this, "User already exists", Toast.LENGTH_SHORT).show()
                    successOrFail.setText("Fail")
                    successOrFail.setTextColor(Color.RED)
                } else {
                    userList[email] = user
                    Toast.makeText(this, "User added successfully", Toast.LENGTH_SHORT).show()

                    successOrFail.setText("Success")
                    successOrFail.setTextColor(Color.GREEN)

                    // Here, number of active user increases by one and its value is set to
                    // relevant text view
                    activeUserNum += 1
                    activeUserEdV.setText(activeUserNum.toString())

                    // showInfoButton becomes clickable since new user is made.
                    showInfoButton.isClickable = true

                }
            } else {
                emailEdT.error = "Email format is not correct"
            }

        } else {
            Toast.makeText(this, "Please fill all fields", Toast.LENGTH_SHORT).show()
        }
    }

    private fun removeUser() {
        val email = emailEdT.text.toString()
        val firstName = firstNameEdT.text.toString()
        val lastName = lastNameEdT.text.toString()
        val age = ageEdT.text.toString()

        if (email.isNotEmpty() && firstName.isNotEmpty() && lastName.isNotEmpty() && age.isNotEmpty()) {
            if (android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
                if (email in userList.keys) {
                    userList.remove(email)
                    Toast.makeText(this, "User removed successfully", Toast.LENGTH_SHORT).show()

                    successOrFail.setText("Success")
                    successOrFail.setTextColor(Color.GREEN)
                    // showInfoButton is not clickable because user is no longer in a map
                    showInfoButton.isClickable = false

                    // If the user is removed number of removed users is increased by one and
                    // number of active users is decreased by one
                    removedUserNum += 1
                    activeUserNum -= 1
                    activeUserEdV.setText(activeUserNum.toString())
                    removedUserEdV.setText(removedUserNum.toString())

                } else {
                    Toast.makeText(this, "User does not exist", Toast.LENGTH_SHORT).show()
                    successOrFail.setText("Fail")
                    successOrFail.setTextColor(Color.RED)
                }
            }
        } else {
            Toast.makeText(this, "Please fill all fields", Toast.LENGTH_SHORT).show()
        }
    }

    private fun updateUser() {

        val email = emailEdT.text.toString()
        val firstName = firstNameEdT.text.toString()
        val lastName = lastNameEdT.text.toString()
        val age = ageEdT.text.toString()

        if (email.isNotEmpty() && firstName.isNotEmpty() && lastName.isNotEmpty() && age.isNotEmpty()) {
            if (android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
                if (email in userList.keys) {
                    // If user inputs their email and changed name, last name and age, new user is
                    // made, which replaces previous user in a map
                    val newUser = User(email, firstName, lastName, age.toInt())
                    userList[email] = newUser
                    Toast.makeText(this, "User updated successfully", Toast.LENGTH_SHORT).show()
                    successOrFail.setText("Success")
                    successOrFail.setTextColor(Color.GREEN)
                    // showInfoButton is once again clickable, because user is in a map.
                    showInfoButton.isClickable = true
                } else {
                    Toast.makeText(this, "User does not exist", Toast.LENGTH_SHORT).show()
                    successOrFail.setText("Fail")
                    successOrFail.setTextColor(Color.RED)
                }
            }
        } else {
            Toast.makeText(this, "Please fill all fields", Toast.LENGTH_SHORT).show()
        }
    }

    private fun showInfo() {
        val email = emailEdT.text.toString()
        val firstName = firstNameEdT.text.toString()
        val lastName = lastNameEdT.text.toString()
        val age = ageEdT.text.toString()

        val intent = Intent(this, SecondActivity::class.java)
        intent.putExtra("email", email)
        intent.putExtra("firstName", firstName)
        intent.putExtra("lastName", lastName)
        intent.putExtra("age", age)
        startActivity(intent)
    }
}


